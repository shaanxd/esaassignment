package com.example.shahid.esaassignment.UserInterface.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shahid.esaassignment.Model.User;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.UserInterface.Dialogs.UpdateEmailDialog;
import com.example.shahid.esaassignment.UserInterface.Dialogs.UpdatePasswordDialog;
import com.example.shahid.esaassignment.Utilities.SharedPrefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends BaseFragment implements IProfileNotifier{

    User mCurrentUser;
    UpdatePasswordDialog mPasswordDialog;
    UpdateEmailDialog mEmailDialog;

    @BindView(R.id.et_userId)
    TextView etUserId;
    @BindView(R.id.et_firstName)
    TextView etFirstName;
    @BindView(R.id.et_lastName)
    TextView etLastName;
    @BindView(R.id.et_email)
    TextView etEmail;
    @BindView(R.id.et_dob_date)
    TextView etDobDate;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, mView);
        setCurrentUser();
        return mView;
    }

    @OnClick(R.id.btn_update_password)
    public void onUpdatePasswordClick() {
        mPasswordDialog = new UpdatePasswordDialog(mContext, mCurrentUser);
        mPasswordDialog.show();
    }

    @OnClick(R.id.btn_update_email)
    public void onUpdateEmailClick() {
        mEmailDialog = new UpdateEmailDialog(mContext, mCurrentUser, this);
        mEmailDialog.show();
    }

    public void setCurrentUser(){
        mCurrentUser = SharedPrefs.getSharedPreference(mContext);
        if (mCurrentUser != null) {
            etUserId.setText(mCurrentUser.getUserID());
            etFirstName.setText(mCurrentUser.getFirstName());
            etLastName.setText(mCurrentUser.getLastName());
            etEmail.setText(mCurrentUser.getUserEmail());
            etDobDate.setText(mCurrentUser.getDOB());
        }
    }
}
