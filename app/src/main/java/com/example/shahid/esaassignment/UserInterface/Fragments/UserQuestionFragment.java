package com.example.shahid.esaassignment.UserInterface.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.shahid.esaassignment.Model.Question;
import com.example.shahid.esaassignment.Model.User;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.UserInterface.Adapters.QuestionAdapter;
import com.example.shahid.esaassignment.Utilities.SharedPrefs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserQuestionFragment extends BaseFragment {

    @BindView(R.id.rvUserQuestion)
    RecyclerView rvUserQuestion;
    QuestionAdapter mQuestionAdapter;

    public UserQuestionFragment() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_user_question, container, false);
        ButterKnife.bind(this, mView);
        User mCurrentUser = SharedPrefs.getSharedPreference(mContext);
        if(mCurrentUser != null) {
            List<Question> mQuestionList = mCurrentUser.getQuestions();
            mQuestionAdapter = new QuestionAdapter(getActivity(), mQuestionList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            rvUserQuestion.setLayoutManager(mLayoutManager);
            rvUserQuestion.setAdapter(mQuestionAdapter);
        }
        return mView;
    }
}
