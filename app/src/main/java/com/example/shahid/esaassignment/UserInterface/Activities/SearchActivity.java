package com.example.shahid.esaassignment.UserInterface.Activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.shahid.esaassignment.Model.Question;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.UserInterface.Adapters.QuestionAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends BaseActivity {

    @BindView(R.id.rvSearchActivity)
    RecyclerView rvSearchActivity;
    QuestionAdapter mQuestionAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_activtity);
        ButterKnife.bind(this);
        setTitle("Search Results");
        setUpAdapter();
    }

    public void setUpAdapter(){
        List<Question> mQuestionList = Question.searchQuestion(getIntent().getStringExtra("mSearchString"));
        mQuestionAdapter = new QuestionAdapter(this, mQuestionList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvSearchActivity.setLayoutManager(mLayoutManager);
        rvSearchActivity.setAdapter(mQuestionAdapter);
    }
}
