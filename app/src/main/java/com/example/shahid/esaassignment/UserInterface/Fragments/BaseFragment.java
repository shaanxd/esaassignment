package com.example.shahid.esaassignment.UserInterface.Fragments;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

public class BaseFragment extends Fragment {

    protected Context mContext;
    protected Activity mActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;

        if(context instanceof Activity){
            mActivity = (Activity) context;
        }
    }

}
