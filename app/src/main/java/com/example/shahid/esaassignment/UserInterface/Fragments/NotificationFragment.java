package com.example.shahid.esaassignment.UserInterface.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.shahid.esaassignment.Model.Notification;
import com.example.shahid.esaassignment.Model.User;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.UserInterface.Adapters.NotificationAdapter;
import com.example.shahid.esaassignment.Utilities.SharedPrefs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends BaseFragment {

    @BindView(R.id.rv_notification)
    RecyclerView rvNotification;
    NotificationAdapter mAdapter;

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, mView);
        setUpAdapter();
        return mView;
    }

    public void setUpAdapter() {
        User mCurrentUser = SharedPrefs.getSharedPreference(mContext);
        if(mCurrentUser != null) {
            List<Notification> mNotificationList = mCurrentUser.getNotifications();
            mAdapter = new NotificationAdapter(mNotificationList, getActivity());
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            rvNotification.setLayoutManager(mLayoutManager);
            rvNotification.setAdapter(mAdapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpAdapter();
    }
}
