package com.example.shahid.esaassignment.UserInterface.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shahid.esaassignment.Model.Question;
import com.example.shahid.esaassignment.Model.User;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.Utilities.SharedPrefs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddQuestionActivity extends BaseActivity {

    User mCurrentUser;

    @BindView(R.id.etDescription)
    EditText etDescription;
    @BindView(R.id.etHeading)
    EditText etHeading;
    @BindView(R.id.btnAddQuestion)
    Button btnAddQuestion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_question);
        ButterKnife.bind(this);
        setTitle("Add Question");
        mCurrentUser = SharedPrefs.getSharedPreference(this);
    }

    @OnClick(R.id.btnAddQuestion)
    public void onAddQuestionClick(View mView){
        if(etDescription.getText().toString().isEmpty()||etHeading.getText().toString().isEmpty()){
            Toast.makeText(this,"Please fill the text fields.", Toast.LENGTH_SHORT).show();
        }
        else{
            SimpleDateFormat mFormat = new SimpleDateFormat("dd EEE, MMMM YY", Locale.US);
            Question mQuestion = new Question(mCurrentUser, etHeading.getText().toString(), etDescription.getText().toString(), mFormat.format(new Date()));
            mQuestion.save();
            finish();
        }
    }

}
