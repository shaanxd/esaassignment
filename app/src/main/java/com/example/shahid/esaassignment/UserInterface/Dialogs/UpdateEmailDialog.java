package com.example.shahid.esaassignment.UserInterface.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shahid.esaassignment.Model.User;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.UserInterface.Fragments.IProfileNotifier;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.widget.Toast.LENGTH_SHORT;

public class UpdateEmailDialog extends Dialog {

    private User mCurrentUser;
    private Context mContext;
    private IProfileNotifier mProfileNotifier;

    @BindView(R.id.tv_currentEmail)
    TextView tvCurrentEmail;
    @BindView(R.id.et_newEmail)
    EditText etNewEmail;
    @BindView(R.id.et_Password)
    EditText etPassword;

    public UpdateEmailDialog(Context context, User user, IProfileNotifier profileNotifier) {
        super(context);
        mContext = context;
        mCurrentUser = user;
        mProfileNotifier = profileNotifier;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_update_email);
        ButterKnife.bind(this);
        tvCurrentEmail.setText(mCurrentUser.getUserEmail());
    }

    @OnClick(R.id.btn_update_email)
    public void onUpdateEmailClick() {
        if (!etNewEmail.getText().toString().isEmpty() && !etPassword.getText().toString().isEmpty()){
            if(!Patterns.EMAIL_ADDRESS.matcher(etNewEmail.getText().toString()).matches())
            {
                Toast.makeText(getContext(), "Invalid Email format", Toast.LENGTH_SHORT).show();
            }else
            if(!User.checkUserExist(etNewEmail.getText().toString())) {
                if (mCurrentUser.validateUser(etPassword.getText().toString())) {
                    mCurrentUser.updateEmail(etNewEmail.getText().toString());
                    Toast.makeText(mContext, "Email updated successfully.", LENGTH_SHORT).show();
                    mProfileNotifier.setCurrentUser();
                    this.dismiss();
                } else {
                    Toast.makeText(mContext, "Invalid Password", LENGTH_SHORT).show();
                }
            }
            else{
                Toast.makeText(mContext, "Email already exists", LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(mContext, "Enter an email address.", LENGTH_SHORT).show();
        }
    }
}
