package com.example.shahid.esaassignment.Utilities;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Encrypt {

    public static String Encrypt(String mPassword) {
        MessageDigest mDigest = null;
        try {
            mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(mPassword.getBytes(),0,mPassword.length());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        assert mDigest != null;
        return(new BigInteger(1,mDigest.digest()).toString(16));
    }
}
