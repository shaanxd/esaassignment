package com.example.shahid.esaassignment.Utilities;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.shahid.esaassignment.Model.User;
import com.google.gson.Gson;

import static android.content.Context.MODE_PRIVATE;


public class SharedPrefs {

    private static String USER_FILE = "USER_INFO";
    private static String USER_INFO = "LOGGED_USER";

    public static  User getSharedPreference(Context mContext){
        SharedPreferences mPrefs = mContext.getSharedPreferences(USER_FILE,MODE_PRIVATE);
        String mJson = mPrefs.getString(USER_INFO, "");
        User mUser = new Gson().fromJson(mJson, User.class);
        if(mUser!=null) {
            return (User.getUser(mUser.getUserID()));
        }
        return null;
    }

    public static void putSharedPreference(Context mContext, User mUser){
        SharedPreferences mPrefs = mContext.getSharedPreferences(USER_FILE,MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mPrefs.edit();
        String json = new Gson().toJson(mUser);
        mEditor.putString(USER_INFO, json);
        mEditor.apply();
    }

    public static void clearSharedPreferences(Context mContext){
        SharedPreferences mPrefs = mContext.getSharedPreferences(USER_FILE,MODE_PRIVATE);
        SharedPreferences.Editor mEditor = mPrefs.edit();
        mEditor.clear().apply();
    }

}
