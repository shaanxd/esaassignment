package com.example.shahid.esaassignment.UserInterface.Activities;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shahid.esaassignment.Model.User;
import com.example.shahid.esaassignment.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity {

    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_passwordRepeat)
    EditText etPasswordRepeat;
    @BindView(R.id.et_firstName)
    EditText etFirstName;
    @BindView(R.id.et_lastName)
    EditText etLastName;
    @BindView(R.id.et_userId)
    EditText etUserId;
    @BindView(R.id.et_dob_date)
    EditText etDobDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        setTitle("Register");
    }

    @OnClick(R.id.btn_register)
    public void onRegisterClick(View mView) {
        if(!etUserId.getText().toString().isEmpty()&& !etFirstName.getText().toString().isEmpty()&& !etLastName.getText().toString().isEmpty()&&
               !etEmail.getText().toString().isEmpty()&& !etPassword.getText().toString().isEmpty()&& !etPasswordRepeat.getText().toString().isEmpty())
        {
            if (User.checkUserExist(etUserId.getText().toString())) {
                Toast.makeText(this, "User ID already in use.", Toast.LENGTH_SHORT).show();
            } else if (User.checkUserExist(etEmail.getText().toString())){
                Toast.makeText(this, "Email ID already in use.", Toast.LENGTH_SHORT).show();
            }else if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches())
            {
                Toast.makeText(this, "Invalid Email format", Toast.LENGTH_SHORT).show();
            }
            else if (!etPassword.getText().toString().equals(etPasswordRepeat.getText().toString())) {
                Toast.makeText(this, "Passwords should be identical.", Toast.LENGTH_LONG).show();
            } else {
                User mUser = new User(etUserId.getText().toString(), etFirstName.getText().toString(),
                        etLastName.getText().toString(), etEmail.getText().toString(), etPassword.getText().toString(),
                        etDobDate.getText().toString());
                mUser.save();
                Toast.makeText(this, "Successfully registered.", Toast.LENGTH_LONG).show();

                finish();
            }
        }
        else
        {
            Toast.makeText(this, "Fields are empty", Toast.LENGTH_SHORT).show();
        }
    }
}
