package com.example.shahid.esaassignment.Model;

import com.orm.SugarRecord;

public class Notification extends SugarRecord {

    private User mUser;
    private Question mQuestion;
    private String mDescription;
    private String mDateTime;
    private boolean isRead;

    public Notification() {
    }

    public Notification(User mUser, Question mQuestion, String mDescription, String mDateTime, boolean isRead) {
        this.mUser = mUser;
        this.mQuestion = mQuestion;
        this.mDescription = mDescription;
        this.mDateTime = mDateTime;
        this.isRead = isRead;
    }

    public Question getQuestion() {
        return mQuestion;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getDateTime() {
        return mDateTime;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean mVal) {
        isRead = mVal;
    }
}
