package com.example.shahid.esaassignment.UserInterface.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.shahid.esaassignment.Model.Question;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.UserInterface.Activities.AddQuestionActivity;
import com.example.shahid.esaassignment.UserInterface.Adapters.QuestionAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {

    @BindView(R.id.rvQuestionList)
    RecyclerView rvQuestionList;
    @BindView(R.id.btn_addPost)
    Button btnAddQuestion;


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, mView);
        setAdapter();
        return mView;
    }

    public void setAdapter(){
        QuestionAdapter mAdapter = new QuestionAdapter(mContext, Question.getAllQuestions());
        rvQuestionList.setAdapter(mAdapter);
        rvQuestionList.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    @OnClick(R.id.btn_addPost)
    public void onAddQuestionClick(View v){
        Intent mIntent = new Intent(mContext, AddQuestionActivity.class);
        startActivity(mIntent);
    }

    @Override
    public void onResume(){
        super.onResume();
        setAdapter();
    }

}
