package com.example.shahid.esaassignment.Model;

import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

public class Question extends SugarRecord {

    private User mUser;
    private String mQuestionHeading;
    private String mQuestionDescription;
    private String mQuestionDate;

    public Question() {
    }

    public Question(User mUser, String mQuestionHeading, String mQuestionDescription, String mQuestionDate) {
        this.mUser = mUser;
        this.mQuestionHeading = mQuestionHeading;
        this.mQuestionDescription = mQuestionDescription;
        this.mQuestionDate = mQuestionDate;
    }

    public List<Comment> getComments() {
        return Select.from(Comment.class).where(Condition.prop("m_question").eq(this)).and(Condition.prop("m_is_nested").eq(0)).list();
    }

    public long getNumberOfComments(){
        return Select.from(Comment.class).where(Condition.prop("m_question").eq(this)).count();
    }

    public static Question getQuestion(long mQuestionID) {
        return Question.findById(Question.class, mQuestionID);
    }

    public void updateQuestion(String mQuestionDescription) {
        this.setQuestionDescription(mQuestionDescription);
        this.save();
    }

    public static List<Question> searchQuestion(String mString) {
        return Select.from(Question.class).where(Condition.prop("m_question_heading").like("%"+mString+"%")).or(Condition.prop("m_question_description").like("%"+mString+"%")).list();
    }

    public static List<Question> getAllQuestions() {

        return listAll(Question.class);
    }

    public User getUser() {
        return mUser;
    }

    public String getQuestionHeading() {

        return mQuestionHeading;
    }

    public String getQuestionDescription() {

        return mQuestionDescription;
    }

    public void setQuestionDescription(String mQuestionDescription) {
        this.mQuestionDescription = mQuestionDescription;
    }

    public String getQuestionDate() {

        return mQuestionDate;
    }

}
