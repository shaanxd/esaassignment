package com.example.shahid.esaassignment.UserInterface.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.shahid.esaassignment.Model.Question;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.UserInterface.Activities.QuestionActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.ViewHolder> {

    private Context mContext;
    private List<Question> mQuestionList;

    public QuestionAdapter(Context mContext, List<Question> mQuestionList) {
        this.mContext = mContext;
        this.mQuestionList = mQuestionList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.adapter_question, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Question mQuestion = mQuestionList.get(position);

        holder.tvQuestionHeading.setText(mQuestion.getQuestionHeading());
        holder.tvQuestionDescription.setText(mQuestion.getQuestionDescription());
        holder.tvUsername.setText(mQuestion.getUser().getUserID());
        holder.tvCommentNum.setText(String.valueOf(mQuestion.getNumberOfComments()));
        holder.llQuestionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(mContext, QuestionActivity.class);
                mIntent.putExtra("mQuestionID", mQuestion.getId());
                mContext.startActivity(mIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mQuestionList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvUsername)
        TextView tvUsername;
        @BindView(R.id.tvQuestionHeading)
        TextView tvQuestionHeading;
        @BindView(R.id.tvQuestionDescription)
        TextView tvQuestionDescription;
        @BindView(R.id.llQuestionLayout)
        LinearLayout llQuestionLayout;
        @BindView(R.id.tvCommentNum)
        TextView tvCommentNum;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
