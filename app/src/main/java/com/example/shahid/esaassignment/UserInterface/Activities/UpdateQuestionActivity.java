package com.example.shahid.esaassignment.UserInterface.Activities;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shahid.esaassignment.Model.Question;
import com.example.shahid.esaassignment.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateQuestionActivity extends BaseActivity {

    Question mSelectedQuestion;

    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tvDateTime)
    TextView tvDateTime;
    @BindView(R.id.tv_updateQuestionActivity_title)
    TextView tvUpdateQuestionActivityTitle;
    @BindView(R.id.et_updateQuestionActivity_description)
    EditText etUpdateQuestionActivityDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_question);
        ButterKnife.bind(this);
        setTitle("Update Question");
        setUpQuestionDetails();
    }

    private void setUpQuestionDetails() {
        mSelectedQuestion = Question.getQuestion(getIntent().getLongExtra("mQuestionID", 0));
        tvUsername.setText(mSelectedQuestion.getUser().getUserID());
        tvDateTime.setText(mSelectedQuestion.getQuestionDate());
        tvUpdateQuestionActivityTitle.setText(mSelectedQuestion.getQuestionHeading());
        etUpdateQuestionActivityDescription.setText(mSelectedQuestion.getQuestionDescription());
    }

    @OnClick(R.id.btn_updateQuestionActivity_update)
    public void onUpdateQuestionClick() {
        if (!etUpdateQuestionActivityDescription.getText().toString().isEmpty()){
            mSelectedQuestion.updateQuestion(etUpdateQuestionActivityDescription.getText().toString());
            Toast.makeText(this, "Question description updated.", Toast.LENGTH_SHORT).show();
            this.onBackPressed();
        }
        else
        {
            Toast.makeText(this, "No description to update", Toast.LENGTH_SHORT).show();
        }
    }
}
