package com.example.shahid.esaassignment.UserInterface.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shahid.esaassignment.Model.User;
import com.example.shahid.esaassignment.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdatePasswordDialog extends Dialog {

    private User mCurrentUser;
    private Context mContext;

    @BindView(R.id.et_oldPassword)
    EditText etOldPassword;
    @BindView(R.id.et_newPassword)
    EditText etNewPassword;

    public UpdatePasswordDialog(Context context, User user) {
        super(context);
        mContext = context;
        mCurrentUser = user;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_update_password);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_update_password)
    public void onUpdatePasswordClick() {
        if (!etOldPassword.getText().toString().isEmpty() && !etNewPassword.getText().toString().isEmpty()){
            if (mCurrentUser.updatePassword(etOldPassword.getText().toString(), etNewPassword.getText().toString())){
                Toast.makeText(mContext, "Password updated successfully.", Toast.LENGTH_SHORT).show();
                this.dismiss();
            }
            else {
                Toast.makeText(mContext, "Old password is invalid", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(mContext, "Fields cannot be empty.", Toast.LENGTH_SHORT).show();
        }
    }
}
