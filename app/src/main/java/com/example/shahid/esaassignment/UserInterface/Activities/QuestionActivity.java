package com.example.shahid.esaassignment.UserInterface.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shahid.esaassignment.Model.Comment;
import com.example.shahid.esaassignment.Model.Notification;
import com.example.shahid.esaassignment.Model.Question;
import com.example.shahid.esaassignment.Model.User;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.UserInterface.Adapters.CommentAdapter;
import com.example.shahid.esaassignment.Utilities.SharedPrefs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QuestionActivity extends BaseActivity {

    User mCurrentUser;
    Question mSelectedQuestion;

    @BindView(R.id.tvUsername)
    TextView tvUsername;
    @BindView(R.id.tvDateTime)
    TextView tvDateTime;
    @BindView(R.id.recycler_questionActivity_comments)
    RecyclerView rvCommentList;
    @BindView(R.id.questionActivity_title)
    TextView tvQuestionTitle;
    @BindView(R.id.questionActivity_description)
    TextView tvQuestionDescription;
    @BindView(R.id.etCommentText)
    EditText etCommentText;
    @BindView(R.id.btn_edit_question)
    Button btnEditQuestion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        ButterKnife.bind(this);
        mCurrentUser = SharedPrefs.getSharedPreference(this);
        if(mCurrentUser!=null) {
            setTitle(mCurrentUser.getUserID() + "'s Question");
        }
        setUpQuestion();
        setUpList();
    }

    public void setUpQuestion() {

        mSelectedQuestion = Question.getQuestion(getIntent().getLongExtra("mQuestionID", 0));

        tvUsername.setText(mSelectedQuestion.getUser().getUserID());
        tvDateTime.setText(mSelectedQuestion.getQuestionDate());
        tvQuestionTitle.setText(mSelectedQuestion.getQuestionHeading());
        tvQuestionDescription.setText(mSelectedQuestion.getQuestionDescription());

        if(!mSelectedQuestion.getUser().getId().equals(mCurrentUser.getId())){
            btnEditQuestion.setVisibility(View.GONE);
        }
    }

    public void setUpList() {
        rvCommentList.setLayoutManager(new LinearLayoutManager(this));
        rvCommentList.setAdapter(new CommentAdapter(this, mSelectedQuestion.getComments()));
    }

    @OnClick(R.id.btnAddComment)
    public void onAddCommentClick(View mView) {
        if(!etCommentText.getText().toString().isEmpty()) {
            SimpleDateFormat mFormat = new SimpleDateFormat("dd EEE, MMMM YY", Locale.US);
            String mDate = mFormat.format(new Date());
            Comment mComment = new Comment(mCurrentUser, mSelectedQuestion, etCommentText.getText().toString(), mDate);
            mComment.save();
            etCommentText.setText("");
            setUpList();
            if (!mSelectedQuestion.getUser().getId().equals(mCurrentUser.getId())) {
                String mDescription = mCurrentUser.getUserID() + " replied to your Question.";
                Notification mNotification = new Notification(mSelectedQuestion.getUser(), mSelectedQuestion, mDescription, mDate, false);
                mNotification.save();
            }
        }
        else{
            Toast.makeText(this, "Please enter a comment", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpQuestion();
        setUpList();
    }

    @OnClick(R.id.btn_edit_question)
    public void onEditQuestionClick() {
        Intent mIntent = new Intent(this, UpdateQuestionActivity.class);
        mIntent.putExtra("mQuestionID", mSelectedQuestion.getId());
        startActivity(mIntent);
    }
}
