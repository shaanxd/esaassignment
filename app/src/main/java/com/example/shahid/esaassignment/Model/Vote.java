package com.example.shahid.esaassignment.Model;

import com.orm.SugarRecord;

public class Vote extends SugarRecord {

    private User mUser;
    private Comment mComment;
    private Boolean mIsUpvote;

    public Vote(){

    }

    public Vote(User mUser, Comment mComment, Boolean mIsUpvote) {
        this.mUser = mUser;
        this.mComment = mComment;
        this.mIsUpvote = mIsUpvote;
    }

    public Boolean getIsUpVote() {
        return mIsUpvote;
    }

    public void setIsUpVote(Boolean mIsUpvote) {
        this.mIsUpvote = mIsUpvote;
    }
}
