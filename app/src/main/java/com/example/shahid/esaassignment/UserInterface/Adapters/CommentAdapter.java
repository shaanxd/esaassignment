package com.example.shahid.esaassignment.UserInterface.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.shahid.esaassignment.Model.Comment;
import com.example.shahid.esaassignment.Model.User;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.UserInterface.Activities.CommentActivity;
import com.example.shahid.esaassignment.Utilities.SharedPrefs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private Context mContext;
    private List<Comment> mCommentList;
    private User mUser;

    public CommentAdapter(Context mContext, List<Comment> mCommentList) {
        this.mContext = mContext;
        this.mCommentList = mCommentList;
        this.mUser = SharedPrefs.getSharedPreference(mContext);
    }

    @NonNull
    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.adapter_comment, parent, false);
        return new CommentAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull final CommentAdapter.ViewHolder holder, int position) {
        final Comment mComment = Comment.getComment(mCommentList.get(position).getId());

        holder.tvUserName.setText(mComment.getUser().getUserID());
        holder.tvComment.setText(mComment.getReplyComment());
        holder.tvVotes.setText(String.valueOf(mComment.getVote()));
        holder.rvCommentReplies.setLayoutManager(new LinearLayoutManager(mContext));

        int mResult = mComment.isUpVoted(mUser);

        holder.btnUpvote.setOnCheckedChangeListener(null);
        holder.btnDownvote.setOnCheckedChangeListener(null);
        holder.tvDateTime.setText(mComment.getReplyDate());

        if(mResult == 1){
            holder.btnUpvote.setChecked(true);
            holder.btnDownvote.setChecked(false);
        }
        else if(mResult == -1){
            holder.btnDownvote.setChecked(true);
            holder.btnUpvote.setChecked(false);
        }
        else{
            holder.btnDownvote.setChecked(false);
            holder.btnUpvote.setChecked(false);
        }

        if(mComment.isNested() || mComment.getNestedCommentCount()==0){
            holder.rvCommentReplies.setVisibility(View.GONE);
        }

        if(mComment.isNested()){
            holder.llReply.setVisibility(View.GONE);
        }
        else {
            holder.rvCommentReplies.setAdapter(new CommentAdapter(mContext, mComment.getNestedComments()));
            holder.tvReply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mIntent = new Intent(mContext, CommentActivity.class);
                    mIntent.putExtra("mCommentID", mComment.getId());
                    mContext.startActivity(mIntent);
                }
            });
        }
        holder.btnUpvote.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mComment.upVoteComment(mUser);
                notifyItemChanged(holder.getAdapterPosition());
            }
        });

        holder.btnDownvote.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mComment.downVoteComment(mUser);
                notifyItemChanged(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCommentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.btnUpvote)
        ToggleButton btnUpvote;
        @BindView(R.id.btnDownVote)
        ToggleButton btnDownvote;
        @BindView(R.id.tvComment)
        TextView tvComment;
        @BindView(R.id.tvUsername)
        TextView tvUserName;
        @BindView(R.id.tvReply)
        LinearLayout tvReply;
        @BindView(R.id.rvCommentReplies)
        RecyclerView rvCommentReplies;
        @BindView(R.id.tvVotes)
        TextView tvVotes;
        @BindView(R.id.replyLayout)
        LinearLayout llReply;
        @BindView(R.id.tvDateTime)
        TextView tvDateTime;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
