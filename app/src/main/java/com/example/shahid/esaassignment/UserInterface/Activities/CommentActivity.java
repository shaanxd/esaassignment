package com.example.shahid.esaassignment.UserInterface.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.shahid.esaassignment.Model.Comment;
import com.example.shahid.esaassignment.Model.Notification;
import com.example.shahid.esaassignment.Model.User;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.Utilities.SharedPrefs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommentActivity extends BaseActivity {

    Comment mSelectedComment;
    User mCurrentUser;

    @BindView(R.id.tvUsername)
    TextView tvUsername;
    @BindView(R.id.tvReply)
    TextView tvReply;
    @BindView(R.id.etNestedReply)
    EditText etNestedReply;
    @BindView(R.id.btnAddComment)
    Button btnAddComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);
        setTitle("Replying to");

        mSelectedComment = Comment.getComment(getIntent().getLongExtra("mCommentID",0));
        mCurrentUser = SharedPrefs.getSharedPreference(this);

        tvUsername.setText(mSelectedComment.getUser().getUserID());
        tvReply.setText(mSelectedComment.getReplyComment());
    }

    @OnClick(R.id.btnAddComment)
    public void onAddCommentClick(View mView){
        String mReply = etNestedReply.getText().toString();
        if(mReply.isEmpty()){
            Toast.makeText(this, "Text Fields Cannot Be Empty", Toast.LENGTH_SHORT).show();
        }
        else{
            SimpleDateFormat mFormat = new SimpleDateFormat("dd EEE, MMMM YY", Locale.US);
            String mDate = mFormat.format(new Date());
            Comment mComment = new Comment(mCurrentUser, mSelectedComment.getQuestion(),mSelectedComment, mReply, mDate);
            mComment.save();
            if(!mSelectedComment.getQuestion().getUser().getId().equals(mCurrentUser.getId())){
                String mPostDesc = mCurrentUser.getUserID() + " replied to a comment on your Question.";
                Notification mPostNoti = new Notification(mSelectedComment.getQuestion().getUser(), mSelectedComment.getQuestion(), mPostDesc, mDate, false);
                mPostNoti.save();
            }
            if(!mSelectedComment.getUser().getId().equals(mCurrentUser.getId())){
                String mCommentDesc = mCurrentUser.getUserID() + " replied to your comment.";
                Notification mCommentNoti = new Notification(mSelectedComment.getUser(), mSelectedComment.getQuestion(), mCommentDesc, mDate, false);
                mCommentNoti.save();
            }
            finish();
        }
    }


}
