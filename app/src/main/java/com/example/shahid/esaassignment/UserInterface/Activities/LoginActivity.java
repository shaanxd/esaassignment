package com.example.shahid.esaassignment.UserInterface.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shahid.esaassignment.Model.User;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.Utilities.SharedPrefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    Intent mIntent;
    User mCurrentUser;
    Context mContext = this;

    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.btn_register)
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setTitle("Login");
        if(SharedPrefs.getSharedPreference(this)!=null){
            mIntent = new Intent(this, HomeActivity.class);
            startActivity(mIntent);
            finish();
        }
    }

    @OnClick(R.id.btn_login)
    public void onLoginClick(View mView) {

       if(User.checkUserExist(etUsername.getText().toString()))
       {
           User mUser = User.getUser(etUsername.getText().toString());
           if(mUser.validateUser(etPassword.getText().toString())) {
               SharedPrefs.putSharedPreference(this, mUser);
               mIntent = new Intent(this, HomeActivity.class);
               startActivity(mIntent);
               finish();
           }
           else{
               Toast.makeText(this, "Invalid Password", Toast.LENGTH_SHORT).show();
           }
       }
       else{
           Toast.makeText(this, "Invalid User Id", Toast.LENGTH_SHORT).show();
       }
    }

    @OnClick(R.id.btn_register)
    public void onBtnRegisterClicked() {
        mIntent = new Intent(this, RegisterActivity.class);
        startActivity(mIntent);
    }
}