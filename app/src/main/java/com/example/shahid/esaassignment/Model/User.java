package com.example.shahid.esaassignment.Model;

import com.example.shahid.esaassignment.Utilities.Encrypt;
import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

public class User extends SugarRecord {

    private String mUserID;
    private String mFirstName;
    private String mLastName;
    private String mUserEmail;
    private String mUserPassword;
    private String mDOB;

    public User(){

    }

    public User(String mUserID, String mFirstName, String mLastName, String mUserEmail, String mUserPassword, String mDOB) {
        this.mUserID = mUserID;
        this.mFirstName = mFirstName;
        this.mLastName = mLastName;
        this.mUserEmail = mUserEmail;
        this.mUserPassword = Encrypt.Encrypt(mUserPassword);
        this.mDOB = mDOB;
    }

    public boolean validateUser(String mUserPassword) {
        if (getUserPassword().equals(Encrypt.Encrypt(mUserPassword))){
            return true;
        }
        return false;
    }

    public boolean updatePassword(String mOldPassword, String mNewPassword) {
        if(getUserPassword().equals(Encrypt.Encrypt(mOldPassword))){
            setUserPassword(Encrypt.Encrypt(mNewPassword));
            save();
            return true;
        }
        return false;
    }

    public void updateEmail(String mNewEmail) {
        this.setUserEmail(mNewEmail);
        save();
    }

    public static boolean checkUserExist(String mString) {
        User mUser = Select.from(User.class)
                .where((Condition.prop("m_user_id").eq(mString)))
                .or(Condition.prop("m_user_email").eq(mString))
                .first();
        return mUser != null;
    }

    public static User getUser(String mUserID){
        return Select.from(User.class)
                .where((Condition.prop("m_user_id").eq(mUserID)))
                .first();
    }

    public List<Question> getQuestions(){
        return Select.from(Question.class).where(Condition.prop("m_user").eq(this)).list();
    }

    public List<Notification> getNotifications(){
        return Select.from(Notification.class).where(Condition.prop("m_user").eq(this)).list();
    }

    public String getUserID() {
        return mUserID;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public String getUserEmail() {
        return mUserEmail;
    }

    public void setUserEmail(String mUserEmail) {
        this.mUserEmail = mUserEmail;
    }

    public String getUserPassword() {
        return mUserPassword;
    }

    public void setUserPassword(String mUserPassword) {
        this.mUserPassword = mUserPassword;
    }

    public String getDOB() {
        return mDOB;
    }

}
