package com.example.shahid.esaassignment.UserInterface.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.shahid.esaassignment.Model.User;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.UserInterface.Fragments.HomeFragment;
import com.example.shahid.esaassignment.UserInterface.Fragments.NotificationFragment;
import com.example.shahid.esaassignment.UserInterface.Fragments.ProfileFragment;
import com.example.shahid.esaassignment.UserInterface.Fragments.UserQuestionFragment;
import com.example.shahid.esaassignment.Utilities.SharedPrefs;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

        @BindView(R.id.search_view) MaterialSearchView mSearchView;
        @BindView(R.id.drawer_layout) DrawerLayout mDrawer;
        @BindView(R.id.nav_view) NavigationView mNavigationView;
        @BindView(R.id.toolbar) Toolbar mToolbar;

        User mCurrentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        ActionBarDrawerToggle mToggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(mToggle);
        mToggle.syncState();

        mSearchView.setOnQueryTextListener(mQueryListener);
        mNavigationView.setNavigationItemSelectedListener(this);

        mCurrentUser = SharedPrefs.getSharedPreference(this);

        View mHeaderView = mNavigationView.getHeaderView(0);

        TextView mUsername = mHeaderView.findViewById(R.id.tvUsername);
        String mUserString = mCurrentUser.getFirstName()+" "+mCurrentUser.getLastName();
        mUsername.setText(mUserString);

        TextView mUserEmail = mHeaderView.findViewById(R.id.tvUserEmail);
        mUserEmail.setText(mCurrentUser.getUserEmail());

        setUpFragment(new HomeFragment(), mNavigationView.getMenu().findItem(R.id.nav_home));
    }

    private void setUpFragment(Fragment mFragment, MenuItem item){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if(fragmentManager.findFragmentById(R.id.fragmentcontainer)!=null) {
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right);
        }
        fragmentTransaction.replace(R.id.fragmentcontainer, mFragment);
        fragmentTransaction.commit();
        setTitle(item.getTitle());
        item.setChecked(true);
    }

    @Override
    public void onBackPressed() {

        if(mSearchView.isSearchOpen()){
            mSearchView.closeSearch();
        }
        else if(mDrawer.isDrawerOpen(GravityCompat.START)){
            mDrawer.closeDrawer(GravityCompat.START);
        }
        else if(getSupportFragmentManager().findFragmentById(R.id.fragmentcontainer).getClass() != HomeFragment.class){
            setUpFragment(new HomeFragment(), mNavigationView.getMenu().findItem(R.id.nav_home));
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem mItem) {
        // Handle navigation view item clicks here.
        int mMenuID = mItem.getItemId();
        Fragment mSelectedFragment;
        Class mFragmentClass = null;

        switch (mMenuID){
            case R.id.nav_home: mFragmentClass = HomeFragment.class;
            break;
            case R.id.nav_mypost: mFragmentClass = UserQuestionFragment.class;
            break;
            case R.id.nav_profile: mFragmentClass = ProfileFragment.class;
            break;
            default: Logout();
        }
        if(mFragmentClass != null && mFragmentClass != getSupportFragmentManager().findFragmentById(R.id.fragmentcontainer).getClass() )
        {
            try {
                mSelectedFragment = (Fragment) mFragmentClass.newInstance();
                setUpFragment(mSelectedFragment, mItem);
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void Logout()
    {
        SharedPrefs.clearSharedPreferences(this);
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        mSearchView.setMenuItem(item);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_noti) {
            setUpFragment(new NotificationFragment(), item);
        }
        return super.onOptionsItemSelected(item);
    }

    MaterialSearchView.OnQueryTextListener mQueryListener
            = new MaterialSearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String mQuery) {
            Intent mSearchIntent = new Intent(
                    HomeActivity.this, SearchActivity.class);
            mSearchIntent.putExtra("mSearchString", mQuery);
            startActivity(mSearchIntent);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
    };
}
