package com.example.shahid.esaassignment.Model;

import com.orm.SugarRecord;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

public class Comment extends SugarRecord {

    private User mUser;
    private Question mQuestion;
    private Comment mComment;
    private String mReplyComment;
    private String mReplyDate;
    private int mVote;
    private boolean mIsNested;

    public Comment() {
    }

    public Comment(User mUser, Question mQuestion, String mReplyComment, String mDate) {
        this.mUser = mUser;
        this.mQuestion = mQuestion;
        this.mReplyComment = mReplyComment;
        this.mReplyDate = mDate;
        this.mVote = 0;
        this.mIsNested = false;
    }

    public Comment(User mUser, Question mQuestion, Comment mComment, String mReplyComment, String mDate) {
        this.mUser = mUser;
        this.mQuestion = mQuestion;
        this.mComment = mComment;
        this.mReplyComment = mReplyComment;
        this.mReplyDate = mDate;
        this.mIsNested = true;
    }

    public List<Comment> getNestedComments() {
        return Select.from(Comment.class).where(Condition.prop("m_comment").eq(this)).list();
    }

    public long getNestedCommentCount(){
        return Select.from(Comment.class).where(Condition.prop("m_comment").eq(this)).and(Condition.prop("m_is_nested").eq(1)).count();
    }

    public void upVoteComment(User mUser){
        int mResult = isUpVoted(mUser);
        if(isUpVoted(mUser)==0){
            Vote mVote = new Vote(mUser, this, true);
            mVote.save();
            setVote(getVote()+1);
        }
        else{
            Vote mVote = Select.from(Vote.class).where(Condition.prop("m_user").eq(mUser)).and(Condition.prop("m_comment").eq(this)).first();
            if(mVote.getIsUpVote()){
                setVote(getVote()-1);
                mVote.delete();
            }
            else{
                setVote(getVote()+2);
                mVote.setIsUpVote(true);
                mVote.save();
            }
        }
        this.save();
    }

    public void downVoteComment(User mUser) {
        if(isUpVoted(mUser)==0){
            Vote mVote = new Vote(mUser, this, false);
            mVote.save();
            setVote(getVote()-1);
        }
        else{
            Vote mVote = Select.from(Vote.class).where(Condition.prop("m_user").eq(mUser)).and(Condition.prop("m_comment").eq(this)).first();
            if(!mVote.getIsUpVote()){
                setVote(getVote()+1);
                mVote.delete();
            }
            else{
                setVote(getVote()-2);
                mVote.setIsUpVote(false);
                mVote.save();
            }
        }
        this.save();
    }

    public static Comment getComment(Long mCommentID){
        return Comment.findById(Comment.class, mCommentID);
    }

    public int isUpVoted(User mUser){
        Vote mVote = Select.from(Vote.class).where(Condition.prop("m_user").eq(mUser)).and(Condition.prop("m_comment").eq(this)).first();
        if(mVote!=null){
            if(mVote.getIsUpVote()){
                return 1;
            }
            return -1;
        }
        return 0;
    }

    public User getUser() {
        return mUser;
    }

    public Question getQuestion() {
        return mQuestion;
    }

    public void setQuestion(Question mQuestion) {
        this.mQuestion = mQuestion;
    }

    public boolean isNested() {
        return mIsNested;
    }

    public String getReplyComment() {
        return mReplyComment;
    }

    public String getReplyDate() {
        return mReplyDate;
    }

    public int getVote() {
        return mVote;
    }

    public void setVote(int mVote) {
        this.mVote = mVote;
    }

}
