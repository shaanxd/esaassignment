package com.example.shahid.esaassignment.UserInterface.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.shahid.esaassignment.Model.Notification;
import com.example.shahid.esaassignment.R;
import com.example.shahid.esaassignment.UserInterface.Activities.QuestionActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private List<Notification> mNotificationList;
    private Context mContext;

    public NotificationAdapter(List<Notification> mNotificationList, Context mContext) {
        this.mNotificationList = mNotificationList;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.adapter_notification, parent, false);
        return new ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Notification mNotification = mNotificationList.get(position);

        if(!mNotification.isRead()){
            holder.itemNotification.setBackgroundResource(R.drawable.nonread_background);
        }
        holder.tvDateTime.setText(mNotification.getDateTime());
        holder.tvDescription.setText(mNotification.getDescription());

        holder.itemNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(mContext, QuestionActivity.class);
                mIntent.putExtra("mQuestionID", mNotification.getQuestion().getId());
                mNotification.setRead(true);
                mNotification.save();
                mContext.startActivity(mIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mNotificationList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvDescription)
        TextView tvDescription;
        @BindView(R.id.tvDateTime)
        TextView tvDateTime;
        @BindView(R.id.item_notification)
        LinearLayout itemNotification;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
